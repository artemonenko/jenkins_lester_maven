package lester.pages;


import java.io.File;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.firefox.FirefoxDriver;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


/*
0661864246 - Саша
0955789631 - Антон
0507125525 - Вика
0951702079 - Аня
0663033934 - Ира
0508246084 - Dasha
0937320128 - Sony
0937878742 -Ira Life
0994247092 - Аня Мартыненко*/



public class BolidovTestcases {

    public static WebDriver driver;
    public static String phone = "0994247092"; //Аня


    public String ProductPage ="http://bolidov.com.ua/product/shiny-cordiant-comfort-r13-175-70-82t-letnjaja/id4784";




    @BeforeClass
    public void setup(){

        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
        //driver.manage().window().maximize();
        //driver.navigate().to("http://bolidov.com.ua/");
        //driver.manage().deleteAllCookies();
    }

    @AfterClass
    public void teardown() throws InterruptedException{
        Thread.sleep(180000);
        driver.quit();
    }

    @Test(priority=1)
    public void zakazZvonka() throws InterruptedException {

        driver.get("http://bolidov.com.ua");

        driver.findElement(By.id("newCallbackInit")).click();
        driver.findElement(By.id("backPhone")).click();
        driver.findElement(By.id("backPhone")).sendKeys(phone);
        driver.findElement(By.className("hunterSubmit")).click();
        Thread.sleep(4000);
        //  if(driver.getPageSource().contains("Производится вызов. Подождите пока Ваш телефон зазвонит! Если телефон не позвонил в течении минуты, попробуйте ещё раз.")){
        System.out.println("Bolidov_zakazZvonka:done");
			/* }
				 else {
					 System.out.println("Bolidov_zakazZvonka:fail");
			 }*/

    }


    @Test(priority=2)
    public void bisrtiyZakaz() throws InterruptedException {


        driver.get(ProductPage);

        driver.findElement(By.xpath("html/body/div[3]/div/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/a[2]")).click();
        driver.findElement(By.className("input_mask_pop")).click();
        driver.findElement(By.className("input_mask_pop")).sendKeys("0000000003");
        driver.findElement(By.xpath(".//*[@id='fast-zak']/div/div[1]/input[2]")).click();
        System.out.println("bisrtiyZakaz:done");

    }

    @Test(priority=3)
    public void zakaz() throws InterruptedException {



        driver.get(ProductPage);
        driver.findElement(By.xpath("html/body/div[3]/div/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/a[1]")).click();
        driver.findElement(By.xpath(".//*[@id='cartControls']/div[3]/div/div[2]/div/a")).click();
        driver.findElement(By.className("enterReg")).click();
        driver.findElement(By.xpath(".//*[@name='login']")).sendKeys("zolotarevskaya.i.wezom@gmail.com");
        driver.findElement(By.xpath(".//*[@name='password']")).sendKeys("12345678");
        driver.findElement(By.id("enterSite")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath(".//*[@id='saveMyData']")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath(".//*[@id='it']")).click();
        driver.findElement(By.xpath("html/body/div[3]/div/div/div[3]/div/div[4]/div/form/div/div/div[5]/div[1]/div/div[1]/div/div[1]/label")).click();
        driver.findElement(By.xpath(".//*[@name='address00']")).sendKeys("К. Маркса, 18 (напротив Укртелекома)");
        driver.findElement(By.xpath(".//*[@id='saveMyData']")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath(".//*[@id='saveMyData']")).click();
        if(driver.getPageSource().contains("Товариство з обмеженною відповідальністю")) {
            System.out.println("zakaz:done");
        }
        else {
            System.out.println("zakaz:fail");
        }


    }





}
