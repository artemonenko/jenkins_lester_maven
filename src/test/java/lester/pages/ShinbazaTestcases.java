package lester.pages;


import java.io.File;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/*0661864246 - Саша
0955789631 - Антон
0507125525 - Вика
0663033934 - Ира
0508246084 - Dasha
0951702079 - Аня
0994247092 - Аня 2
0663033934 //Ира
0508246084 - Dasha
0937320128 - Sony
0669610285 - Dima
0937878742 -Ira Life*/



public class ShinbazaTestcases    {

    public WebDriver driver;
    public WebDriverWait wait;
    public  DateFormat df;
    public  Date dateobj ;



    String phoneNumber = "0500000000";
    String email ="test@test.ua";
    String login = "test.wezom@mail.ru";
    String password= "123456";



    String linkpage ="http://shinbaza.com.ua/1306/legkovaja-shina-summer-175-70-r13-82-t";
    String personalPhoneNumber = "0508246084"; //Dasha


    @BeforeClass
    public void setup(){
        driver = new FirefoxDriver();
        wait = new WebDriverWait(driver, 25);
        driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
        driver.navigate().to("http://shinbaza.com.ua ");
        driver.manage().deleteAllCookies();
        System.out.println("deleteAllCookies");
    }

    @AfterClass
    public void teardown() throws InterruptedException{
        Thread.sleep(180000);
        driver.quit();
    }

	/*
	public static void curentTime(){
		 df = new SimpleDateFormat("dd-MM-yy HH.mm.ss");
		 dateobj = new Date();
		System.out.println(df.format(dateobj));
		String time = df.format(dateobj);
	}*/



    @Test(priority=1)
    public void OpeningBrowser() throws InterruptedException, IOException{
	/*	df = new SimpleDateFormat("dd-MM-yy HH.mm.ss");
		dateobj = new Date();
		System.out.println(df.format(dateobj));
		String time = df.format(dateobj);*/


        System.out.println("OpeningBrowser");
        driver.get("http://shinbaza.com.ua/");
        Assert.assertEquals(driver.getTitle(), "Купить шины в Украине: каталог шин на авто по низким ценам. Летняя резина и зимние покрышки в интернет магазине - ШинБаза");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName("body")));
        //Thread.sleep(2000);
        System.out.println(driver.getTitle());
        System.out.println("OpeningBrowser done");
        driver.manage().window().maximize();
        Thread.sleep(3000);
        //screenshot();

        //File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        //FileUtils.copyFile(scrFile, new File("d:\\screenshot_"+time+".png"));

    }

    @Test(priority=2)

    public void logIn() throws InterruptedException{
        System.out.println("login");
        driver.findElement(By.xpath(".//*[@href='#enterReg']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='enterReg']")));
        driver.findElement(By.xpath(".//*[@name='email']")).sendKeys(login);
        driver.findElement(By.xpath(".//*[@name='password']")).sendKeys(password);
        driver.findElement(By.xpath(".//*[text()='войти']")).click();
        Thread.sleep(1500);
        System.out.println("login done");
        Thread.sleep(1500);
    }


    //����� ������
    @Test(priority=3)
    public void MakeCallBack() throws InterruptedException{

        driver.findElement(By.xpath("(.//*[@id='newCallbackInit'])[2]")).click();
        driver.findElement(By.xpath(".//*[text()='Позвоните мне']")).click(); //- call me button
        driver.findElement(By.xpath(".//*[@id='backPhone']")).sendKeys(personalPhoneNumber);
        driver.findElement(By.xpath(".//*[text()='Позвоните мне']")).click();  // call me again
        System.out.println("Callback done");
        Thread.sleep(3000);
    }


    //������� �����
   @Test(priority=4)
    public void MakeQuickOrder() throws InterruptedException {
        System.out.println("Quick Order");
        driver.get(linkpage);
        Thread.sleep(3000);
        driver.findElement(By.xpath(".//*[@id='mm-0']/div[3]/div[3]/div/div[3]/div/div/div/div[1]/div/div[2]/div/div[3]/a[2]")).click(); // -- click on fast zakaz btn
        driver.findElement(By.xpath(".//*[@id='obrzv2']/div/p/a")).click();
        driver.findElement(By.xpath(".//*[@id='pfso']")).sendKeys(phoneNumber);
        driver.findElement(By.xpath(".//*[@id='obrzv2']/div/p/a")).click();
        System.out.println("Quick Order done");
    }



   @Test(priority=5)
    public void MakeOrder() throws InterruptedException, IOException  {
		/*df = new SimpleDateFormat("dd-MM-yy HH.mm.ss");
		dateobj = new Date();
		System.out.println(df.format(dateobj));
		String time = df.format(dateobj);
		*/
        driver.get(linkpage);
        System.out.println("Order");
        driver.navigate().refresh();
        Thread.sleep(1500);
        driver.findElement(By.xpath("(.//*[@class='buy addToCart'])[2]")).click();
        driver.findElement(By.xpath(".//*[@href='/cart/personal_data']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@class='btn-red btnShadow submit cartNext wSubmit']")));


      //  driver.findElement(By.xpath(".//*[@id='tid']")).clear();
      //  driver.findElement(By.xpath(".//*[@id='tid']")).sendKeys("Тестовый заказ");
        driver.findElement(By.xpath(".//*[@class='btn-red btnShadow submit cartNext wSubmit']")).click();
        Thread.sleep(2000);

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='np']"))); //��������
        driver.findElement(By.xpath(".//*[@id='np']")).click();
        driver.findElement(By.xpath(".//*[@name='delivery2']")).sendKeys("Отделение №1: просп. Адмирала Сенявина, 27");
        driver.findElement(By.xpath(".//*[@class='btn-red btnShadow submit cartNext wSubmit']")).click();
        Thread.sleep(1500);
        driver.findElement(By.xpath(".//*[@class='btn-red btnShadow submit cartNext wSubmit']")).click(); //third step submit button
        System.out.println("Order done");
        System.out.println("Tetscase done");
        //driver.close();
        //File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        //FileUtils.copyFile(scrFile, new File("d:\\screenshot_"+time+".png"));

    }




}
